import { Component, OnInit } from '@angular/core';
import { Input } from '@angular/core';


@Component({
  selector: 'app-etudiant',
  templateUrl: './etudiant.component.html',
  styleUrls: ['./etudiant.component.scss']
})
export class EtudiantComponent implements OnInit {
  @Input() nomEtu = 'Jean';
  @Input() prenomEtu = "Martin"
  @Input() commentaires = "Defaut commentaires";
  @Input() statuAbs = false; 
  constructor() { }
  
  retNomEtu(){
    return this.nomEtu;
  }
  retPrenomEttud() {
    return this.prenomEtu;
  }

  ngOnInit() {
  }

}
