import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  estAuth = false;
  title = 'Seance TP2 (reprise)';
  soustitre = "Prise en main première application "
  commentaireSeanceTP = "Test initialisation";

  tabEtudiant = [{nom: 'Jean', prenom: 'Paul', status: true},
                 {nom: 'Anne', prenom: 'Laure', status: false},
                 {nom: 'Pierre', prenom: 'Martin', status: true}];

 constructor(){

    setTimeout(() => {this.estAuth = true }, 3000 );
 }

 getCommentaires() {
   if (this.commentaireSeanceTP != "grosMot"){
     return  this.commentaireSeanceTP;
   }else{
    return  "Mot Interdit !!! ";
   }
 }
 rendTousPres(){
   console.log("Ca marche ...");
 }
}
